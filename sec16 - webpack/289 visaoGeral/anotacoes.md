# Tópicos

- Webpack é baseado em sistema de módulos
- No JS há 2 principais sistemas de módulos:
  - CommonJS (Node é baseado nele)
  - ES Module
- Nessas aulas sobre webpack, serão vistos ambos sistemas

## Características (imagem no site do webpack)

- Módulos com suas dependências
- Entry point (nó/node principal)
  - propriedade especificada no arquivo de configuração
  - é um arquivo js (já que vai usar sistema de módulo do JS)
  - esse arquivo de entrada faz suas respectivas referências pra outros arquivos
  - esses outros arquivos referenciam outros, conforme o caso
  - ao final, esse arquivo de entrada é passado para o webpack
  - o webpack, por sua vez (através do sistema de módulos):
    - navegará por todos os arquivos da aplicação
    - fará o carregamento do arquivo (operação de module loader):
      - baseado na extensão do arquivo
      - aplicando a lógica específica e necessária para aquele renderizar aquele tipo de arquivo (módulo)

## Loaders

- No webpack, faz-se o registro de vários loaders
- os loaders podem ser aplicados para observarem:
  - um grupo de extensões de arquivos por toda a aplicação
  - uma extensão de arquivos, apenas, por toda a aplicação
- ao encontrar um tipo de extensão, o webpack:
  - passa o arquivo para o loader registrado (configurado) p/ aquele tipo de arquivo
  - o loader específico 'faz o trabalho que tem que ser feito' (processará aquele tipo de arquivo)
  - tem o arquivo final gerado ao final
    - são os assets/recursos estáticos da aplicação, da maneira como serão providos pelo servidor
    - são consumidos nativamente pelo browser

## Arquivos desejados

- Para os arquivos serem observados pelo webpack, devem ser importados:
  - no entry point
  - em qualquer outro arquivo, que por sua vez, seja importado no entry point

## Outras características do Webpack

- Tem um servidor web
  - faz o carregamento automático, sempre quando há mudança em algum arquivo
  - faz o build automático, por padrão (sem necessidade de configurar um watcher)
