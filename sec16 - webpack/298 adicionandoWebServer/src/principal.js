import Pessoa from './pessoa';
// ao importar um diretório, o padrão é importar o index.js que houver nele
import './assets';

const atendente = new Pessoa();
console.log(atendente.cumprimentar());
