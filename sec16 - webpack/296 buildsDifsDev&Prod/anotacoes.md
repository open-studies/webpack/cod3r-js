# Tópicos

- O build para ambiente de desenvolvimento gera arquivos extensos e com espaços não otimizados para trafegarem na rede.
- O build para ambiente de produção gera arquivos minificados, sem espaços sobrando dentro do código. É otimizado para trafegar na rede.
- É possível configurar o webpack para gerar arquivos para ambos os ambientes.
