// esse arquivo padrão de configuração do webpack, por ser arquivo js, é interpretado pelo node
// aqui também se usa o sistema de módulos que o node também entende
const webpack = require('webpack');
module.exports = {
  mode: 'development',
  entry: './src/principal.js'
};
