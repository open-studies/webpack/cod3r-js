# Tópicos

## Sistemas de módulos

- Apesar de em um mesmo projeto com webpack poder ser usada a sintaxe de mais de um sistema de módulos, como o ES Modules e o CommonJS, essa não é uma boa prática.
- É recomendável seguir um padrão em cada projeto.
