# Tópicos

- Os assets, no ambiente de desenvolvimento, são os recursos estáticos que fazem parte do desenvolvimento da aplicação
- Para cada novo tipo de arquivo importado na aplicação, deve-se cadastrar o loader que estará habilitado para manipulá-lo
