const webpack = require('webpack');
module.exports = {
  mode: 'development',
  entry: './src/principal.js',
  output: {
    filename: 'principal.js',
    path: __dirname + '/public',
  },
  module: {
    // aqui estarão os loaders
    rules: [
      {
        test: /\.css$/,
        // um tipo de arquivo pode usar mais de um loader
        // todo loader especificado deve estar também no roll de dependências
        use: [
          'style-loader', // add CSS, injetando um elemento <style> no DOM (ou seja, css inline) manipulado pelo arquivo JS principal (não gera arquivo CSS). não costuma ser uma maneira muito recomendada.
          'css-loader', // interpreta @import, url(), etc no CSS ....
        ],
      },
    ],
  },
};
