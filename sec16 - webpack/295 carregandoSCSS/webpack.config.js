const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
module.exports = {
  mode: 'development',
  entry: './src/principal.js',
  output: {
    filename: 'principal.js',
    path: __dirname + '/public',
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'estilo.css',
    }),
  ],
  module: {
    // loaders
    rules: [
      {
        // testar o regex seguinte no https://regex101.com/
        test: /\.s?[ac]ss$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
      },
    ],
  },
};
